import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, Provider } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { AuthService } from './shared/services/auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { CartComponent } from './shared/components/cart/cart.component';
import { DropdownModule } from 'primeng/dropdown';
import { HomePageComponent } from  './home-page/home-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { MenubarModule } from 'primeng/menubar';
import { MessageService } from 'primeng/api';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { RegistrationComponent } from './registration/registration.component';
import { RestaurantCardComponent } from './shared/components/restaurant-card/restaurant-card.component';
import { RestaurantComponent } from './shared/components/restaurant/restaurant.component';
import { RestaurantsComponent } from './shared/components/restaurants/restaurants.component';
import { ToastModule } from 'primeng/toast';
import { AuthGuard } from './shared/guards/auth.guard';

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor
}


@NgModule({
  declarations: [
    AppComponent,
    RestaurantsComponent,
    RestaurantCardComponent,
    RestaurantComponent,
    MainLayoutComponent,
    HomePageComponent,
    RegistrationComponent,
    LoginPageComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    MenubarModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    RatingModule,
    ButtonModule,
    AppRoutingModule,
    HttpClientModule,
    DropdownModule,
    RadioButtonModule,
    ToastModule
  ],
  providers: [INTERCEPTOR_PROVIDER, AuthService, MessageService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
