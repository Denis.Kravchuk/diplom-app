import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

import { AuthService } from '../shared/services/auth.service';
import { RestaurantService } from '../shared/services/restaurant.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

 constructor(
  private restaurantService: RestaurantService,
  public authService: AuthService,
  private messageService: MessageService
  ) {}

  ngOnInit() {}

}
