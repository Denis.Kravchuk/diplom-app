import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Menu } from '../../shared/models/menu.model';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-menu',
  templateUrl: './create-menu.component.html',
  styleUrls: ['./create-menu.component.scss']
})
export class CreateMenuComponent {
  restaurantId = this.route.snapshot.paramMap.get('id');
  photoUrl: string = '';

form: FormGroup
  submitted = false

  constructor(
    private restaurantService: RestaurantService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
        name: new FormControl(null, []),
        ingredients: new FormControl(null, []),
        price: new FormControl(null, []),
      }
    );
  }
  createMenu() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true

    const menu: Menu  = {
      name: this.form.value.name,
      ingredients: this.form.value.ingredients,
      price: this.form.value.price,
      photoUrl: this.photoUrl,
      restaurantId: this.restaurantId as string
    }

    this.restaurantService.createMenu(menu).subscribe( () => {
      this.form.reset();
      this.submitted = false;
    })
  }

  getMenuPhoto() {
    this.restaurantService.getMenuPhoto(this.form.get('name')?.value).subscribe((photoUrl: string) => {
      this.photoUrl = photoUrl;
    });
   }
}
