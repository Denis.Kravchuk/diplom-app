import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthGuard } from '../shared/guards/auth.guard';
import { AuthService } from '../shared/services/auth.service';
import { CommonModule } from "@angular/common";
import { HttpClientModule } from '@angular/common/http';
import { MenubarModule } from 'primeng/menubar';
import { NgModule } from "@angular/core";
import { RestaurantService } from '../shared/services/restaurant.service';
import { RouterModule } from "@angular/router";
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ImageModule } from 'primeng/image';
import { ProfileComponent } from './profile.component';
import { OrderComponent } from './order/order.component';
import { ToastModule } from 'primeng/toast';
import { AvatarModule } from 'primeng/avatar';
import { ProfileLayoutComponent } from './shared/components/profile-layout/profile-layout.component';
import { MenuModule } from 'primeng/menu';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { RatingModule } from 'primeng/rating';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';

@NgModule ({
    declarations: [
      ProfileLayoutComponent,
      ProfileComponent,
      OrderComponent,
      DashboardPageComponent,
      CreatePageComponent,
      CreateMenuComponent,
      EditPageComponent
    ],
    imports: [
        CommonModule,
        CardModule,
        RatingModule,
        ButtonModule,
        DropdownModule,
        RadioButtonModule,
        MenubarModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ImageModule,
        ToastModule,
        AvatarModule,
        MenuModule,
        RouterModule.forChild([
            {
                path: '', component: ProfileLayoutComponent, children: [
                  {path: '', component: ProfileComponent},
                  {path: 'order', component: OrderComponent},
                  {path: 'dashboard', component: DashboardPageComponent},
                  {path: 'create', component: CreatePageComponent},
                  {path: 'restaurant/:id/add-menu', component: CreateMenuComponent},
                  {path: 'restaurant/:id/edit', component: EditPageComponent}
                ]
            }
        ])
    ],
    exports: [RouterModule],
    providers: [AuthGuard, RestaurantService, MessageService]
})
export class ProfileModule {
}
