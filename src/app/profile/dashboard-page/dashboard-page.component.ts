import { Component, OnDestroy, OnInit } from '@angular/core';

import { AuthService } from '../../shared/services/auth.service';
import { Restaurant } from '../../shared/models/restaurant.model';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { Subscription } from 'rxjs';
import { Menu } from 'src/app/shared/models/menu.model';



@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})


export class DashboardPageComponent implements OnInit, OnDestroy {

  restaurants: Restaurant[] = []
  pSub: Subscription
  dSub: Subscription
  menus: Menu[] = [];

  constructor(
    private restaurantService: RestaurantService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.pSub = this.restaurantService.getAll().subscribe((restaurants: Restaurant[]) => {
    this.restaurants = restaurants;
    this.getMenus();
   })
  }

  ngOnDestroy(): void {
    if (this.pSub) {
      this.pSub.unsubscribe()
     }
     if (this.dSub) {
      this.dSub.unsubscribe()
     }
   }

  getMenus() {
    this.restaurantService.getMenus().subscribe(
      (menus: Menu[]) => {
        console.log(menus)
        this.menus = menus;
      });
  }

  removeRestaurant(id: any) {
   this.dSub = this.restaurantService.remove(id).subscribe(() => {
      this.restaurants = this.restaurants.filter(restaurant => restaurant.id !== id)
    });
   }

   removeMenu(id: string) {
    this.restaurantService.removeMenu(id).subscribe(() => {
      this.menus = this.menus.filter((menu: Menu) => menu?.id !== id);
    });
   }
}

