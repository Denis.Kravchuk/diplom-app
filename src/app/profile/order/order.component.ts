import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { take, tap } from 'rxjs';

import { AuthService } from '../../shared/services/auth.service';
import { Order } from '../../shared/models/order.model';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { Menu } from 'src/app/shared/models/menu.model';



@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  orders: Order[] = [];
  total: number = 0;
  menus: any;

  constructor(
    private restaurantService: RestaurantService,
    public authService: AuthService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getPreorder();
  }

  getPreorder() {
    this.restaurantService.getOrders().pipe(
      take(1),
      tap((orders: Order[]) => {
        return this.restaurantService.orders = orders;
      })
    ).subscribe(() => {
      this.getUserOrder();
      this.getTotalPrice();
    });
  }
  removeOrderDish(order: Order, id: string) {
    this.restaurantService.removeMenu(id).subscribe(() => {
      order.menus = order.menus.filter((menu: Menu) => menu?.id !== id);
    });
   }

  private getUserOrder() {
    const ordersSnapshot = JSON.parse(JSON.stringify(this.restaurantService.orders));
    this.orders = ordersSnapshot.filter((order: Order) => {
      return order.userId === this.authService.currentUser.id});
  }

  private getTotalPrice() {
    this.total = this.orders.reduce((acc, order) => acc + this.getOrderPrice(order), 0);
  }

  private getOrderPrice(order: Order): number {
    return order?.menus.reduce((acc, menu) => acc + +menu.price, 0);
  }
}
