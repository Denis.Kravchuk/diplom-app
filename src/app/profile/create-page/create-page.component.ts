import { FormControl, FormGroup } from '@angular/forms';

import { AuthService } from '../../shared/services/auth.service';
import { Component } from '@angular/core';
import { Restaurant } from '../../shared/models/restaurant.model';
import { RestaurantService } from '../../shared/services/restaurant.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent {


form: FormGroup
  submitted = false

  constructor(
    private restaurantService: RestaurantService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
   this.form = new FormGroup({
     name: new FormControl(null, []),
     description: new FormControl(null, []),
     photo: new FormControl(null, []),
   }
   )
  }
  createRestaurant() {
    console.log(this.form)
    if (this.form.invalid) {
      return
    }

    this.submitted = true

    const restaurant: Restaurant  = {
      name: this.form.value.name,
      description: this.form.value.description,
      userId: this.authService.currentUser.id,
      photo: this.form.value.photo
    }

    this.restaurantService.create(restaurant).subscribe( () => {
      this.form.reset()
      this.submitted = false
    })
  }
}
