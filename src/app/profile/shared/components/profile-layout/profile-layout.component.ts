import { Component, OnInit } from "@angular/core";

import { AuthService } from "../../../../shared/services/auth.service";
import { MenuItem } from "primeng/api";
import { ROLE } from "src/app/shared/models/user.model";

@Component({
  selector: 'app-profile-layout',
  templateUrl: './profile-layout.component.html',
  styleUrls: ['./profile-layout.component.scss']
})
export class ProfileLayoutComponent implements OnInit {
  mainMenu: MenuItem[];

  constructor(
    private authService: AuthService
  ){
  }

  ngOnInit() {
    this.mainMenu = [
      {
        label: 'Профіль',
        routerLink: '/profile'
      },
      {
        label: 'Мої замовлення',
        routerLink: '/profile/order'
      }
    ];

    this.setAdminMenu();
  }

  private setAdminMenu() {
    const adminMenu = [
      {
        label: 'Ресторани',
        routerLink: '/profile/dashboard',
      },
      {
        label: 'Створити ресторан',
        routerLink: '/profile/create',
      },
      {
        label: 'Реєстрація',
        routerLink: '/registration'
      }
    ];
    console.log(this.authService.currentUser);
    if (this.authService.currentUser.role === ROLE.ADMIN) {
      this.mainMenu.push(...adminMenu);
    }
  }
}
