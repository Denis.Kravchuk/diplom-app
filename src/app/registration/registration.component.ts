import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ROLE, User } from '../shared/models/user.model';

import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  ROLE = ROLE;
  roles: ROLE[] = [ROLE.BUYER, ROLE.ADMIN, ROLE.RESTAUEATEUR]
  selectedRole: ROLE

   hideConfirnPassword: boolean = true;
   form: FormGroup = new FormGroup({
    confirmPassword:  new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    role: new FormControl(ROLE.BUYER, [Validators.required])
   })

   constructor(
      public authService: AuthService,
      private router: Router
    ) {}

  ngOnInit(): void {
  }
  signupWithRole(){
    if (this.form.invalid) {
      return
    }
    const user: User = {
      email: this.form.value.email,
      password: this.form.value.password,
      role: this.form.value.role,
      returnSecureToken: false
    }
    this.authService.signupWithRole(user).subscribe( () => {
      this.form.reset();
      this.router.navigate(['/profile', 'dashboard']);
    })
  }
}


