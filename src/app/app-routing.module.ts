import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { CartComponent } from './shared/components/cart/cart.component';
import { HomePageComponent } from  './home-page/home-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { NgModule } from '@angular/core';
import { RegistrationComponent } from './registration/registration.component';
import { RestaurantComponent } from './shared/components/restaurant/restaurant.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent, children: [
    {path: '', redirectTo: '/', pathMatch: 'full'},
    {path: '', component: HomePageComponent},
    {path: 'restaurant/:id', component: RestaurantComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'login', component: LoginPageComponent},
    {path: 'cart', component: CartComponent},
    {
      path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuard]
    }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
