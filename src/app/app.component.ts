import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable, switchMap, take, tap } from 'rxjs';

import { AuthService } from './shared/services/auth.service';
import { User } from './shared/models/user.model';
import { RestaurantService } from './shared/services/restaurant.service';
import { Preorder } from './shared/models/preorder.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  user$: Observable<User>

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private restaurantService: RestaurantService
  ) {}

  ngOnInit(): void {
    this.setUsers();
    this.getPreorder();
  }

  setUsers() {
    if (this.authService.currentUser) {
      this.authService.getAllUsers().pipe(
        take(1),
        tap((users: User[]) => this.authService.users = users)
      ).subscribe();
    }
  }

  getPreorder() {
    this.restaurantService.getPreorders().pipe(
      take(1),
      tap((preorders: Preorder[]) => {
        return this.restaurantService.preorders = preorders
      })
    ).subscribe();
  }
}

