import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../shared/services/auth.service';
import { User } from '../shared/models/user.model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form: FormGroup
  submitted = false
  message: string
  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
   this.route.queryParams.subscribe((params: Params) => {
    if (params['loginAgain']) {
     this.message = 'Введіть дані'
    } else if (params['authFailed']) {
      this.message = 'Введіть дані ще раз'
    }
   })

   this.form = new FormGroup({
     email: new FormControl(null, []),
     password: new FormControl(null, []),
   }
   )
  }
  userLogin() {
    if (this.form.invalid) {
      return
    }

    this.submitted = true

    const user: User = {
      email: this.form.value.email,
      password: this.form.value.password,
      returnSecureToken: false
    }

    this.authService.login(user).subscribe( () => {
      this.form.reset()
      this.router.navigate(['/'])
      this.submitted = false
    })
  }

  goToRegister() {
    this.router.navigate(['/registration']);
  }
}
