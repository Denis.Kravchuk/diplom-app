import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';

import {AuthService} from '../services/auth.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class AuthGuard {
  constructor(
    private auth: AuthService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isAuthenticated()) {
      return true
    }
    else {
      this.auth.logout()
      this.router.navigate(['/profile', 'login'],
      {
        queryParams: {
          loginAgain: true
        }
      })
      return false
    }
   }
 }

