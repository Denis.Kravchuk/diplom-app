import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Menu } from "../models/menu.model";
import { Observable } from 'rxjs/internal/Observable';
import { Order } from "../models/order.model";
import { Preorder } from "../models/preorder.model";
import { Restaurant } from '../models/restaurant.model';
import { environment } from '../../../environment/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class RestaurantService {
  preorders: Preorder[] = [];
  orders: Order[] = [];

  constructor(private http: HttpClient) {}
  create(restaurant: Restaurant): Observable<Restaurant> {
    return this.http.post(`${environment.fbDBUrl}/restaurants.json`, restaurant)
      .pipe(map((response: any) => {
        return {
          ...restaurant,
          id: response.name,
        }
      }))
}

    getAll(): Observable<Restaurant[]> {
      return this.http.get(`${environment.fbDBUrl}/restaurants.json`)
      .pipe(map((response: {[key: string]: any}) => {
        return Object
        .keys(response)
        .map(key => ({
        ...response[key],
        id: key,
        date: new Date(response[key].date)
        }))
      }))
    }
    getById(id: string): Observable<Restaurant> {
      return this.http.get<Restaurant>(`${environment.fbDBUrl}/restaurants/${id}.json`)
        .pipe(map((restaurant: Restaurant) => {
          return {
            ...restaurant, id
          }
        }))
    }
    remove(id:string): Observable<void> {
      return this.http.delete<void>(`${environment.fbDBUrl}/restaurants/${id}.json`)
    }

    createMenu(menu: Menu): Observable<Menu> {
      return this.http.post(`${environment.fbDBUrl}/menus.json`, menu)
        .pipe(map((response: any) => {
          return {
            ...menu,
            id: response.name,
          }
        }))
  }

  removeMenu(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.fbDBUrl}/menus/${id}.json`);
  }

  getMenus(): Observable<Menu[]> {
    return this.http.get(`${environment.fbDBUrl}/menus.json`)
    .pipe(map((response: {[key: string]: any}) => {
      return Object
      .keys(response)
      .map(key => ({
      ...response[key],
      id: key
      }))
    }))
  }

  getMenuPhoto(key: string): Observable<string> {
    return this.http.get(`https://api.unsplash.com/search/photos?client_id=${environment.unsplashApiKey}&query=${key}`)
    .pipe(map((response: any) => {
      console.log(response.results[0]);
      return response.results[0].urls.small;
    }));
  }

  getPreorders(): Observable<Preorder[]> {
    return this.http.get(`${environment.fbDBUrl}/preorders.json`)
    .pipe(map((response: {[key: string]: any}) => {
      return response ? Object
      .keys(response)
      .map(key => ({
      ...response[key],
      id: key
      })) : [];
    }))
  }

  createPreOrder(preorder: Preorder): Observable<Preorder> {
    console.log(preorder)
    return this.http.post(`${environment.fbDBUrl}/preorders.json`, preorder)
      .pipe(map((response: any) => {
        const currentPreorder = {
          ...preorder,
          id: response.name,
        };
        this.preorders = [currentPreorder];
        return currentPreorder;
      }))
  }

  updatePreOrder(preorder: Preorder): Observable<Preorder> {
    return this.http.patch(`${environment.fbDBUrl}/preorders/${preorder.id}.json`, preorder)
      .pipe(map((response: any) => {
        const currentPreorder = {
          ...preorder,
          id: response.name,
        };
        this.preorders.push(currentPreorder);
        return currentPreorder;
      }))
  }

  removePreorder(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.fbDBUrl}/preorders/${id}.json`);
  }

  createOrder(order: Order): Observable<Order> {
    return this.http.post(`${environment.fbDBUrl}/orders.json`, order)
      .pipe(map((response: any) => {
        const currentOrder = {
          ...order,
          id: response.name,
        };
        this.orders = [currentOrder];
        return currentOrder;
      }))
  }

  getOrders(): Observable<Order[]> {
    return this.http.get(`${environment.fbDBUrl}/orders.json`)
    .pipe(map((response: {[key: string]: any}) => {
      return response ? Object
      .keys(response)
      .map(key => ({
      ...response[key],
      id: key
      })) : [];
    }))
  }

  removeOrder(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.fbDBUrl}/orders/${id}.json`);
  }
}
