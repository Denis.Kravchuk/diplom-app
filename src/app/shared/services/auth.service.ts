import { FbAuthResponse, ROLE, User } from "../models/user.model";
import { Observable, map, switchMap, tap } from "rxjs";

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../../environment/environment.prod';

@Injectable({providedIn: 'root'})
export class AuthService {
  users: User[] = [];
  currentUser: User = JSON.parse(localStorage.getItem('fb-user') as string);

  constructor(private http: HttpClient) {}

  get token(): string {
    const expDate = new Date(localStorage.getItem('fb-token-exp') as string);
    if (new Date() > expDate) {
        this.logout();
        return '';
    }
    return localStorage.getItem('fb-token') as string;
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get(`${environment.fbDBUrl}/users.json`).pipe(
      map((response: any) => {
        return Object.keys(response).map((key: string) => {
          return {
            ...response[key],
            id: key
          } as User;
        });
      }));
  }

  signupWithRole(user: User): Observable<any> {
    return this.http.post(`${environment.fbDBUrl}/users.json`, user)
    .pipe((map((response: any) => {
        return {
          ...user,
          id: response.name,
        }
      })),
      switchMap(() => {
          user.returnSecureToken = true
          return this.signup(user);
      }),
      switchMap((response: any) => {
        return this.getUser(response);
      }))
    }

  login(user: User): Observable<any> {
    user.returnSecureToken = true
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
    .pipe(
        switchMap((response: any) => {
          this.setToken(response);
          return this.getUser(response);
        })
    )
  }

  private getUser(response: User): Observable<User> {
    return this.getAllUsers().pipe(
      map((users: User[]) => {
        this.users = users;
        const currentUser: User = users.find((user: User) => user.email === response.email) as User;
        const shortCurrentUser = {
          id: currentUser.id,
          email: currentUser.email,
          role: currentUser.role
        };
        this.currentUser = shortCurrentUser;
        console.log(this.currentUser);
        this.setUserToLocalStorage(shortCurrentUser);
        return currentUser;
      })
    );
  }

  private setUserToLocalStorage(user: User) {
    localStorage.setItem('fb-user', JSON.stringify(user));
  }

  signup(user: User): Observable<any> {
      user.returnSecureToken = true
      return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.apiKey}`, user)
      .pipe(
          tap((response: any) => {
            if (this.currentUser.role !== ROLE.ADMIN) {
              this.setToken(response);
            }
          })
      )
  }

  logout() {
   this.setToken(null);
   this.currentUser = null;
  }

  isAuthenticated(): boolean {
      return !!this.token
  }

  private setToken(response: FbAuthResponse | null) {
      if (response?.idToken) {
          const expDate = new Date(new Date().getTime() +  +response.expiresIn * 1000)
          localStorage.setItem('fb-token', response.idToken)
          localStorage.setItem('fb-token-exp', expDate.toString());
      } else {
          localStorage.clear()
      }
  }
}
