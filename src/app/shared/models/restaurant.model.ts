export interface Restaurant {
    id?: string;
    name: string;
    description: string;
    userId: string;
    photo: string;
}

export enum PAYMENT {
    CASH = 'Готівка',
    CARD = 'Картка'
}