import { Data } from "@angular/router";
import { Menu } from "./menu.model";

export interface Order {
    id?: string;
    menus: Menu[];
    userId: string;
    date: Data;
    status: ORDER_STATUS;
    payment: string;
}

export enum ORDER_STATUS {
    PROCESSING = 'In processing',
    CONFIRMED = 'Confirmed',
    DELIVERY = 'Delivery'
}