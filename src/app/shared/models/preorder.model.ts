import { Menu } from "./menu.model";

export interface Preorder {
    id?: string;
    menus: Menu[];
    userId: string;
}
