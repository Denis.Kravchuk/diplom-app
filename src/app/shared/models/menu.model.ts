export interface Menu {
    id?: string;
    name: string;
    ingredients: string;
    price: string;
    photoUrl?: string;
    restaurantId: string;
}
