

export interface User {
    id?: string
    email: string
    password?: string
    returnSecureToken?: boolean
    role?: ROLE
}
export interface FbAuthResponse {
    expiresIn: string
    idToken: string
    localId: string
}

export interface FbCreateResponse {
    name: string
}

export enum ROLE {
    BUYER = 'BUYER',
    ADMIN = 'ADMIN',
    RESTAUEATEUR = 'RESTAUEATEUR'
}
