import { Component, OnInit } from '@angular/core';
import { ORDER_STATUS, Order } from '../../models/order.model';
import { Subject, switchMap, take, tap } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { MessageService } from 'primeng/api';
import { PAYMENT } from '../../models/restaurant.model';
import { Preorder } from '../../models/preorder.model';
import { RestaurantService } from '../../services/restaurant.service';
import { Menu } from '../../models/menu.model';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  PAYMENT = PAYMENT;
  payment: string[] = Object.keys(PAYMENT);
  selectedPayment: string = this.payment[0];
  preorder: Preorder;
  total: number = 0;
  destroy$: Subject<any> = new Subject<any>();
  menus: any;

 constructor(
  private restaurantService: RestaurantService,
  public authService: AuthService,
  private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getPreorder();
  }

  getPreorder() {
    this.restaurantService.getPreorders().pipe(
      take(1),
      tap((preorders: Preorder[]) => {
        return this.restaurantService.preorders = preorders;
      })
    ).subscribe(() => {
      this.getUserPreorder();
      this.getTotalPrice();
    });
  }

  private getUserPreorder() {
    this.preorder = this.restaurantService.preorders.find((preorder: Preorder) => {
      return preorder.userId === this.authService.currentUser.id});
  }

  private getTotalPrice() {
    this.total = this.preorder?.menus.reduce((acc, menu) => acc + +menu.price, 0);
  }

  removeCartDish(id: string) {
    this.restaurantService.removeMenu(id).subscribe(() => {
      this.menus = this.menus.filter((menu: Menu) => menu?.id !== id);
    });
   }

  submitCart(preorder: Preorder) {
    const preorderCopy: Preorder = JSON.parse(JSON.stringify(preorder));
    delete preorderCopy.id;
    const today = new Date();
    const order: Order = {
      ...preorderCopy,
      status: ORDER_STATUS.PROCESSING,
      payment: this.selectedPayment,
      date: new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0)
    }
    this.restaurantService.createOrder(order).pipe(
      switchMap(() => {
        return this.restaurantService.removePreorder(preorder.id)
      }),
      tap(() => {
        this.restaurantService.preorders = this.restaurantService.preorders.filter((preorder: Preorder) => preorder.id !== preorder.id);
      })
    ).subscribe(() => {
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Order Submitted' });
      this.preorder = null;
    });
  }
}
