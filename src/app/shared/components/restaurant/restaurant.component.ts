import { Component, Input, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Restaurant } from '../../models/restaurant.model';
import { RestaurantService } from '../../services/restaurant.service';
import { Menu } from '../../models/menu.model';
import { AuthService } from '../../services/auth.service';
import { Preorder } from '../../models/preorder.model';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {

restaurant: Restaurant
restaurantId = this.route.snapshot.paramMap.get('id');
menus: Menu[] = [];

 constructor(
  private restaurantService: RestaurantService,
  private route: ActivatedRoute,
  public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getRestaurant();
    this.getMenus();
  }

  private getRestaurant() {
    if (this.restaurantId) {
      this.restaurantService.getById(this.restaurantId).subscribe(
        (restaurant: Restaurant) => {
          this.restaurant = restaurant;
        });
    }
  }

  getMenus() {
    this.restaurantService.getMenus().subscribe(
      (menus: Menu[]) => {
        console.log(menus)
        this.menus = menus;
      });
  }
  removeDish(id: string) {
    this.restaurantService.removeMenu(id).subscribe(() => {
      this.menus = this.menus.filter((menu: Menu) => menu?.id !== id);
    });
   }

  buyMenu(menu: Menu) {
    let currentUserPreorder: Preorder = this.restaurantService.preorders.find((preorder: Preorder) => {
      return preorder.userId === this.authService.currentUser?.id;
    })
    if (currentUserPreorder) {
      currentUserPreorder.menus.push(menu);
      this.restaurantService.updatePreOrder(currentUserPreorder).subscribe();
    } else {
      currentUserPreorder = {
        menus:[menu],
        userId: this.authService.currentUser?.id
      };
      this.restaurantService.createPreOrder(currentUserPreorder).subscribe();
    }
  }
}
