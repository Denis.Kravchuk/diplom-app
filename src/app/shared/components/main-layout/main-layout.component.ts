import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Subject, takeUntil } from "rxjs";

import { AuthService } from "src/app/shared/services/auth.service";
import { MenuItem } from "primeng/api";
import { ROLE } from "src/app/shared/models/user.model";

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss']
  })
  export class MainLayoutComponent implements OnInit {
    mainMenu: MenuItem[];
    destroy$: Subject<any> = new Subject<any>();

    constructor(
      private authService: AuthService,
      private router: Router
      ) {}

    ngOnInit(): void {
      this.setMainMenu();
      this.router.events.pipe(
        takeUntil(this.destroy$)
      ).subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.setMainMenu();
        }
      });
    }

    private setMainMenu() {
      this.mainMenu = [
        {
          label: 'Головна',
          routerLink: '/',
        }
       ];
       this.setLogoutMenu();
       this.setAdditionalMenu();
    }

    private setAdditionalMenu() {
      if (this.authService.currentUser) {
        const additionalMenu = [
          {
            label: 'Корзина',
            routerLink: '/cart',
          },
          {
            label: 'Профіль',
            routerLink: '/profile',
          }
        ];
        this.mainMenu.splice(this.mainMenu.length - 1, 0, ...additionalMenu);
      }
    }

    private setLogoutMenu() {
      const logoutMenu = {
        label: 'Logout',
        routerLink: '/login',
        command: this.authService.logout.bind(this.authService)
      };
      const loginMenu = {
        label: 'Login',
        routerLink: '/login'
      };
      this.mainMenu.push(this.authService.currentUser ? logoutMenu : loginMenu);
    }
  }
