import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { Restaurant } from "../../models/restaurant.model";
import { RestaurantService } from "../../services/restaurant.service";

@Component({
    selector: 'app-restaurants',
    templateUrl: './restaurants.component.html',
    styleUrls: ['./restaurants.component.scss']
  })
export class RestaurantsComponent {
  restaurants$: Observable<Restaurant[]>

  constructor(private restaurantService: RestaurantService) {}

  ngOnInit(): void {
    this.restaurants$ = this.restaurantService.getAll();
  }
}
